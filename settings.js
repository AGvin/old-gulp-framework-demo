var extend = require('node.extend'),
        path = require('path'),
        rootBuilder = __dirname,
        rootSources = path.join(path.dirname(rootBuilder), 'src'),
        rootDest = path.join(path.dirname(rootBuilder), 'dest');
//--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
var settings = {
    plugins: require('gulp-load-plugins')(),
    paths: {
        root: rootBuilder,
        sources: {
            demo: path.join(rootSources, 'demo'),
            scss: path.join(rootSources, 'scss'),
            js: path.join(rootSources, 'js')
        },
        target: {
            demo: path.join(rootDest, 'demo/'),
            scss: path.join(rootDest, 'scss/'),
            js: path.join(rootDest, 'js/')
        }
    }
};
//--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
var settingsCustom;
try {
    settingsCustom = require('./settings-custom');
} catch (err) {
    settingsCustom = {};
	if(err.code !== 'MODULE_NOT_FOUND') {
		console.log(err);
	}
}

settings = extend(true, settings, settingsCustom);
module.exports = settings;