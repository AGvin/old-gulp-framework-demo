# Gulp Micro Framework Demo

Demo for: https://bitbucket.org/AGvin/gulp-framework

## Installation:
Clone sources from repo:
```
#!bash

git clone https://bitbucket.org/AGvin/gulp-framework-demo
```
And then, in cloned dir, execute:

### For windows:
```
#!bash
npm run do-preinstall-win
npm i
```

###For linux:
```
#!bash
npm run do-preinstall
npm i
```

## HowTo execute demo:

###For windows:
*execute in shell*
```
#!bash
C:\Users\%USERNAME%\AppData\Roaming\npm\gulp
C:\Users\%USERNAME%\AppData\Roaming\npm\gulp another:subtask2
```
where C:\Users\%USERNAME%\AppData\Roaming\npm\gulp - path to gulp executive file, and %USERNAME% this is current user

*for fast run, execute files*
```
#!bash
run_another_subtask2.bat
run_default.bat
```

###For linux:
*execute in shell*
```
#!bash
gulp
gulp another:subtask2
```