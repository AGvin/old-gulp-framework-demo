module.exports = function (gulpFramework) {

	var gulp = gulpFramework.getGulpInst(),
		plugins = gulpFramework.getGulpPlugins(),
		demoPath = './demo-watch/*.txt';

    return gulp.watch([demoPath], function () {
		console.log('gulp.watch DEMO');
    });

};