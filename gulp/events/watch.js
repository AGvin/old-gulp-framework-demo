module.exports = function (watch) {
    watch.on('ready', function(event) {
        console.log('[watcher] WATCH STARTED:', event._patterns);
        //console.log('[watcher] WATCH STARTED:', event._watched);
     })
    .on('change', function(evt) {
        console.log(
            '[watcher] File ' + evt.path + ' was ' + evt.type + ', processing...'
        );
    });
};